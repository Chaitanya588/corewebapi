﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Entities
{
    public static class Role
    {
        public const string Auditor = "Auditor";
        public const string User = "User";
    }
}
