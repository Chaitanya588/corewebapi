﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Dtos;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class AuditorController : ControllerBase
    {

        private IAuditService _auditService;
        private IMapper _mapper;        

        public AuditorController(
            IAuditService auditService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _auditService = auditService;
            _mapper = mapper;            
        }

        [Authorize(Roles = Role.Auditor)]
        [HttpGet]
        public IActionResult GetAll()
        {
            var loginLogs = _auditService.GetAll();
            var loginLogDtos = _mapper.Map<IList<LoginLogDto>>(loginLogs);
            return Ok(loginLogDtos);
        }
    }
}
