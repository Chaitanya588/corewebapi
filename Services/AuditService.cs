﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface IAuditService
    {
        IEnumerable<LoginLog> GetAll();
    }
    public class AuditService : IAuditService
    {
        private DataContext _context;

        public AuditService(DataContext context)
        {
            _context = context;
        }
        public IEnumerable<LoginLog> GetAll()
        {
            return _context.LoginLogs;
        }
    }
}
