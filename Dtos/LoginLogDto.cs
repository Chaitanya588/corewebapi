﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Dtos
{
    public class LoginLogDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string IpAddress { get; set; }
        public string LoginTime { get; set; }
        public string LogoutTime { get; set; }
        public string Token { get; set; }
        public string Role { get; set; }
    }
}
